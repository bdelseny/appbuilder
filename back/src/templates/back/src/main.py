#!/usr/bin/env python
"""Main controller"""

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
def get_hello_world():
    """Get hello world

    :return: "Hello World!"
    :rtype: str
    """
    return "Hello World!"
