FROM python:alpine

WORKDIR /src

COPY ./src /src

RUN pip install --trusted-host pypi.python.org -r requirements.txt

EXPOSE 5000
RUN ls
CMD uvicorn --host 0.0.0.0 --port 5000 main:app --reload --log-level trace --use-colors

