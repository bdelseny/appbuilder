# Back

$app_name back repository.


## Development guide

*Work in progress*

Go to the repository:
```bash
cd back
```

Build docker image using the development dockerfile:

```bash
docker build -f Dev.Dockerfile -t ${technical_name}_back .
```

You may need to launch docker commands as a sudo user if your current user is not on the docker group.
- ```-f Dev.Dockerfile``` argument is used to build the docker image using the development configuration which allow tracing, debugging and live reload. For live reload you will also need to correctly set the ```docker run``` command.
- ```-t libshare_bo``` is used to tag the image, you can name it as you need.

Run the docker image:

```bash
docker run --mount type=bind,source="$(pwd)"/app,target=/app -p 5000:5000 libshare_bo
```

- ```--mount type=bind,source="$(pwd)"/app,target=/app``` is used to bind, ```type=bind```, our local ```app``` development folder, ```source="$(pwd)"/app```, to the docker image ```app``` folder, ```target=/app```. You need to set this argument to allow live reload when changing local development file.
- ```-p 5000:5000``` is used to bind the docker port to your local host port allowing you to open the application on your host at http://localhost:5000/

## Run pytest

- Build the image:

    ```bash
    docker compose -f ./tests.docker-compose.yml up --force-recreate --build -d
    ```

- Launch docker and enter it:

    ```bash
    docker exec -it ${technical_name}_test sh
    ```

- Run linter:

    ```bash
    pylint --max-line-length=120 --output-format=json:pylint.json,colorized --msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" src
    pylint-json2html -o pylint.html pylint.json
    ```

- Run tester:

    ```bash
    pytest --cov=src/ --cov-fail-under=80 --cov-report=html:./coverage --html=test_report.html --self-contained-html --verbose -r A --full-trace
    ```
