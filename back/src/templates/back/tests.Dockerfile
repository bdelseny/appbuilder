# Use an Python docker image with Pandoc and Texlive on it
FROM python:alpine

WORKDIR /src

COPY . /src

RUN pip install -r src/requirements.txt

RUN pip install -r tests/requirements.txt

RUN pip install pytest pytest-cov pytest-html coverage pylint pylint-json2html

CMD sh
