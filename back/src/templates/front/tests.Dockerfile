FROM node:alpine

WORKDIR /app

COPY . /app

RUN npm install

RUN npm install -g eslint jest

CMD sh
