# Libshare Front

## Run eslint

- Build the image:

    ```sh
    docker build -f ./tests.Dockerfile . -t test_${technical_name}_front
    ```

- Launch docker and enter it:

    ```sh
    docker run --rm -ti --mount type=bind,source="$(pwd)",target=/app -w="/app" test_${technical_name}_front sh
    ```

- Run linter:

    ```sh
    npm run lint
    ```

- Run tests:

    ```sh
    npm run test
    ```

- Run coverage:

    ```sh
    npm run coverage
    ```
