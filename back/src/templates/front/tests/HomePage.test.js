/* eslint-disable no-unused-vars */
import { template } from './resources/HomePage.template';
import { HomePage, getWelcomeMessage } from '../public/scripts/views/HomePage';

beforeEach(() => {
  jest.clearAllMocks();
  if (global.BACK_HOST) {
    delete global.BACK_HOST;
  }
});

describe('HomePage page', () => {
  test('name', () => {
    expect(HomePage.name).toEqual('HomePage');
  });
  test('template', () => {
    expect(HomePage.template).toEqual(template);
  });
  test('data', () => {
    expect(HomePage.data()).toEqual({ welcomeMessage: '' });
  });
  test('mounted', () => {
    expect(HomePage.mounted).toEqual(getWelcomeMessage);
  });
});

describe('HomePage functions', () => {
  test('get welcome message', async () => {
    global.BACK_HOST = '';
    let self = { welcomeMessage: '' };
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve('nice'),
      })
    );
    await getWelcomeMessage.call(self);
    expect(self.welcomeMessage).toEqual('nice');
  });
  test('get welcome message error', async () => {
    global.BACK_HOST = '';
    let self = { welcomeMessage: '' };
    global.fetch = jest.fn(() => {
      throw 'Error';
    });
    const spyConsoleError = jest.spyOn(console, 'error');
    await getWelcomeMessage.call(self);
    expect(spyConsoleError).toHaveBeenCalledWith('Error');
  });
});