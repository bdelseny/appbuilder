import { html } from '../../public/scripts/utils/templating';

export const template =
  html`
  <div id="HomePage">
    <h2>Welcome to $app_tile</h2>
    <span>{{ welcomeMessage }}</span>
  </div>`;
