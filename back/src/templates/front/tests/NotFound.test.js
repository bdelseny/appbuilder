/* eslint-disable no-unused-vars */
import { template } from './resources/NotFound.template';
import { NotFound } from '../public/scripts/views/NotFound';

beforeEach(() => {
  jest.clearAllMocks();
  if (global.BACK_HOST) {
    delete global.BACK_HOST;
  }
});

describe('NotFound page', () => {
  test('name', () => {
    expect(NotFound.name).toEqual('NotFound');
  });
  test('template', () => {
    expect(NotFound.template).toEqual(template);
  });
});
