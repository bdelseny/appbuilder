import { HomePage } from './views/HomePage.js';
import { NotFound } from './views/NotFound.js';

const routes = [
  { name: 'HomePage', path: '/', component: HomePage },
  { path: '/:pathMatch(.*)*', name: 'not-found', component: NotFound },
  { path: '/:pathMatch(.*)', name: 'bad-not-found', component: NotFound }
];

/* eslint-disable no-undef */
const router = VueRouter.createRouter({
  history: VueRouter.createWebHashHistory(),
  base: BASE_URL, //store this using vuex ?
  routes
});

const app = Vue.createApp({});

app.use(router);
app.mount('#app');
