import { html } from '../utils/templating.js';

export const HomePage = {
  name: 'HomePage',
  template: html`
  <div id="HomePage">
    <h2>Welcome to $app_name</h2>
    <span>{{ welcomeMessage }}</span>
  </div>`,
  data: function () {
    return { welcomeMessage: '' };
  },
  mounted: getWelcomeMessage
};

export async function getWelcomeMessage() {
  try {
    let response = await fetch(
      // eslint-disable-next-line no-undef
      `${BACK_HOST}/`,
      {
        mode: 'cors',
        method: 'GET'
      }
    );
    this.welcomeMessage = await response.json();
  } catch (error) {
    console.error(error);
  }
}