import { html } from '../utils/templating.js';

export const NotFound = {
  name: 'NotFound',
  // eslint-disable-next-line no-undef
  template: html`
  <div>
    <h1>404 Not found</h1>
    <p>
      The page you meant to reach is not available or you do not have right
      access to it.
    </p>
    <router-link to="/" title="Go back to home page">Go back to home page</router-link>
  </div>`
};
