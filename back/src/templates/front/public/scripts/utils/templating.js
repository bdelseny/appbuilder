/* eslint-disable no-unused-vars */

/**
 * Function which transform a value to template string
 * 
 * This is mostly used to have html syntax higlighting on template strings (using extensions for lit-html).
 * 
 * @param {string} value value to tranform to template
 * @returns a template string
 */
export function html(value) {
  return `${value}`;
}
