#!/usr/bin/env python

import argparse
import json
import os
import re
import logging
from os.path import join, isfile
from string import Template

directory = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser(description="Application builder")

parser.add_argument("-c", "--conf", help="Configuration file as json")
parser.add_argument("-o", "--output", help="Output folder (absolute)")
parser.add_argument(
    '-d', '--debug',
    help="Print lots of debugging statements",
    action="store_const", dest="loglevel", const=logging.DEBUG,
    default=logging.ERROR,
)
parser.add_argument(
    '-v', '--verbose',
    help="Be verbose",
    action="store_const", dest="loglevel", const=logging.INFO,
)
args = parser.parse_args()
logging.basicConfig(level=args.loglevel)

with open(args.conf, 'r') as json_conf_file:
  data=json_conf_file.read()

conf = dict(json.loads(data))

p_folder = re.compile('(node_modules|__pycache__|.pytest_cache|coverage)')
p_file = re.compile('(.npmrc|jest_html_reporters.html|junit.xml|.coverage)')


BASE_PATH = f'{directory}/templates'

def create_file_from_template(parent_folder, path, file, folder="./"):
  """Create a file from a template

  :param parent_folder: the root folder
  :type parent_folder: str
  :param path: original file path
  :type path: str
  :param file: template file name
  :type file: str
  :param folder: template file subfolder, defaults to "./"
  :type folder: str, optional
  """
  file_path = join(path, file)
  logging.debug(f'Old file {file_path}')
  with open(file_path, 'r') as template_file:
    data=template_file.read()
  template = Template(data)
  content = template.safe_substitute(conf['data'])
  new_file = file_path.replace(BASE_PATH, args.output)
  new_file_folder = parent_folder.replace(BASE_PATH, args.output)
  os.makedirs(join(new_file_folder, folder), exist_ok=True)
  logging.debug(f'New file {new_file}')
  with open(new_file, 'w') as out:
    out.write(content)

title = conf['data']['app_name']
logging.info(f'Create application {title}')
for folder, subfolders, folder_files in os.walk(BASE_PATH):
  if (folder is BASE_PATH):
    for file in folder_files:
      create_file_from_template(folder, folder, file)
  for subfolder in subfolders:
    if not p_folder.search(folder) and not p_folder.search(subfolder):
      logging.debug(f'Folder: {subfolder}')
      full_path = join(BASE_PATH, folder, subfolder)
      files = [f for f in os.listdir(full_path) if isfile(join(full_path, f))]
      for file in files:
        if not p_file.search(file):
          create_file_from_template(folder, full_path, file, subfolder)
logging.info(f'Application created in {args.output}')

